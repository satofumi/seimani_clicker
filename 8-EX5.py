import time
import seimani

clicker = seimani.Seimani()

sent_units = False
while clicker.not_quit():

    if clicker.is_counting(500):
        if not sent_units:
            clicker.click_fast_button()

            time.sleep(0.25)
            clicker.click_unit(0)
            time.sleep(0.25)
            clicker.move_unit(0, 444, 574)

            time.sleep(0.25)
            clicker.click_unit(1)
            time.sleep(0.25)
            clicker.click_unit(2)

            sent_units = True

    else:
        sent_units = False

    clicker.click_yes_button_position()

    time.sleep(0.25)
