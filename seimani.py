import sys
import datetime
import pygetwindow as gw
import pyautogui
import pyocr
import win32api, win32con
import numpy as np


class Seimani:
    _window = None
    _quit = False
    _previous_count_image = None
    _previous_counted = datetime.datetime.now() - datetime.timedelta(hours=1)


    def __init__(self):
        windows = gw.getWindowsWithTitle('政剣マニフェスティア')
        if len(windows) == 0:
            print('not found game application.')
            sys.exit()

        self._window = windows[0]
        self._window.activate()

    def not_quit(self):
        return not self._quit

    def is_counting(self, maxValue):
        count_image = pyautogui.screenshot(region=(self._window.topleft.x + 249, self._window.topleft.y + 52, 65, 40))

        counting = False
        # カウント開始を検出した後は、直前の画像と異なっていさえすれば動作中とみなす。
        if self._previous_count_image:
            if not np.array_equal(count_image, self._previous_count_image):
                counting = True
                self._previous_count_image = count_image

        else:
            tool = pyocr.get_available_tools()[0]
            builder = pyocr.builders.DigitBuilder(tesseract_layout=8)
            result = tool.image_to_string(count_image, lang="eng", builder=builder)
            if result.isdigit():
                value = int(result)
                counting = value >= 0 and value < (maxValue - 1)
                if counting:
                    self._previous_count_image = count_image
                    self._previous_counted = datetime.datetime.now()

        # 最後の画像からのカウント検出から数秒間はカウント中とみなす。
        if not counting:
            if datetime.datetime.now() - self._previous_counted < datetime.timedelta(seconds=5):
                counting = True

        if not counting:
            self._previous_count_image = None

        return counting


    def click_fast_button(self):
        self._click(129, 73)

    def click_unit(self, index):
        self._click(1000, (97 * index) + 100)

    def move_unit(self, index, x, y):
        self.click_unit(index)
        self._click(x, y)

    def click_yes_button_position(self):
        self._click(610, 517)

    def _click(self, x, y):
        mx = self._window.topleft.x + x
        my = self._window.topleft.y + y
        pyautogui.moveTo(mx, my)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)

        p = pyautogui.position()
        if abs(p.x - mx) > 4 or (p.y - my) > 4:
            self._quit = True
