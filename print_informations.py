import sys
import time
import pygetwindow as gw
import pyautogui
import pyocr


windows = gw.getWindowsWithTitle('政剣マニフェスティア')
if len(windows) == 0:
    print('not found game application.')
    sys.exit()

window = windows[0]
window.activate()

print(window.topleft.x, window.topleft.y)


count_image = pyautogui.screenshot(region=(window.topleft.x + 249, window.topleft.y + 52, 65, 40))
count_image.save('count.png')
tools = pyocr.get_available_tools()
tool = tools[0]
builder = pyocr.builders.DigitBuilder(tesseract_layout=8)
result = tool.image_to_string(count_image, lang="eng", builder=builder)
print(result)


while True:
    x, y = pyautogui.position()
    cx = x - window.topleft.x
    cy = y - window.topleft.y
    print(cx, cy)
    sys.stdout.flush()
    time.sleep(0.1)
