import time
import seimani

clicker = seimani.Seimani()

sent_units = False
while clicker.not_quit():

    if clicker.is_counting(400):
        if not sent_units:
            clicker.click_fast_button()
            time.sleep(0.25)
            clicker.click_unit(0)
            time.sleep(1.0)
            clicker.click_unit(1)
            sent_units = True

    else:
        sent_units = False

    clicker.click_yes_button_position()

    time.sleep(0.25)
